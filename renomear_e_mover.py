import sys
import unicodedata
import re
import os

GS_COMMAND_SKEL = "/usr/bin/gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH"


def slugify(value, allow_unicode=False):
    """
    Taken from https://github.com/django/django/blob/master/django/utils/text.py
    Convert to ASCII if 'allow_unicode' is False. Convert spaces or repeated
    dashes to single dashes. Remove characters that aren't alphanumerics,
    underscores, or hyphens. Convert to lowercase. Also strip leading and
    trailing whitespace, dashes, and underscores.
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize("NFKC", value)
    else:
        value = (
            unicodedata.normalize("NFKD", value)
            .encode("ascii", "ignore")
            .decode("ascii")
        )
    value = re.sub(r"[^\w\s-]", "", value.lower())
    return re.sub(r"[-\s]+", "-", value).strip("-_")


filelist = []
for subdir, dirs, files in os.walk("."):
    subdir_sanitised = slugify(subdir)
    if subdir == ".":
        continue
    if len(files) > 1:
        print(f"ERROR: {subdir} has more than one file in it! Skipping it.")
    else:
        file = files[0]
        extension = os.path.splitext(file)[-1]
        if extension != ".pdf":
            print(f"ERROR: {subdir} has no PDF file. Skipping it.")
        else:
            # if f[0] == '.' or f[-1] == '~': continue
            relative_path = os.path.join(subdir, file)
            new_relative_path_original = os.path.join(
                ".", f"{subdir_sanitised}.bak.pdf"
            )
            new_relative_path = os.path.join(".", f"{subdir_sanitised}.pdf")
            os.rename(relative_path, new_relative_path_original)
            return_code = os.system(
                f"{GS_COMMAND_SKEL} -sOutputFile={new_relative_path} {new_relative_path_original}"
            )
            if return_code != 0:
                print(f"ERROR: There was a problem with {subdir} when optimising it.")
            else:
                print(f"Finished parsing {subdir}.")
                os.remove(new_relative_path_original)
                os.rmdir(subdir)
