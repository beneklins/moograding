=======================================
Moograding: Moodle grading made quickly
=======================================

Simple set of scripts to speed up assignment grading workflows that use
Xournal++

Overview of the process:
1. Download all PDF files
2. Rename all of them.
3. Fit them into an A4 paper size page
4. Grade them using Xournal++ and following a pattern when inserting the grade
5. Calculate the grades and stamp them

Licence
=======

Moograding is libre software and licensed under the GPL (GNU General Public
licence), either version 3 or (at your option) any later version. See the
bundled LICENCE file for details.
