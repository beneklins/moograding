#!/bin/bash

GRADE_IT="~/física/phd/teaching_assistantship/python/bin/python ~/física/phd/teaching_assistantship/grade-it.py"
PDFSCALE="~/física/library/code_snippets/pdf_manipulation/pdfScale/pdfScale.sh"
export GRADE_IT
export PDFSCALE

function scale_all {
	echo -n "Are you sure you want to scale and fit all documents to A4? [y/N]: "
	read CONTINUE_FLAG
	if [ "${CONTINUE_FLAG}" = "y" ]; then
		# Scale and fit all documents in the current directory.
		# find . -type f -name "*.pdf" -exec sh "${PDFSCALE}" -v -r A4 -c A4 --vert-align bottom -a none -s 0.95 --pdf-settings ebook "{}" \;
		find ./ -type f -name "*.pdf" -exec sh -c 'pdfjam --quiet --outfile /tmp/temp_pdf.pdf --paper a4paper "{}" && mv /tmp/temp_pdf.pdf "${0%.pdf}"_size.pdf' {} \;
		find ./ -type f -name "*_size.pdf" -exec sh -c '${PDFSCALE} -v -r A4 --vert-align bottom -a none -s 0.95 --pdf-settings ebook {} && rm {}' \;
		echo "Done! Please verify any errors."
	else
		echo "Ok, then. Bye!"
	fi
}

function final_grade_all {
	# echo ${MAX_VALUE}
	# ASSIGNMENT_NUMBER=$1
	# MAX_VALUE=$2
	echo -n "Are you sure you want to calculate the grade for assignment ${ASSIGNMENT_NUMBER} whose maximum grade is ${MAX_VALUE} or, with extras, $((${MAX_VALUE} + ${MAX_VALUE_EXTRA}))? [y/N]: "
	read CONTINUE_FLAG
	if [ "${CONTINUE_FLAG}" = "y" ]; then
		# Calculate final grade, generate report file and stamped PDF file.
		find . -type f -name "*.pdf" -exec ${GRADE_IT} "{}" ${ASSIGNMENT_NUMBER} ${MAX_VALUE} ${MAX_VALUE_EXTRA} \;
		echo "Done! Please verify any errors."
	else
		echo "Ok, then. Bye!"
	fi
	# Add mark on top of the first page.
	# https://unix.stackexchange.com/a/614749
	# tmpfile=$(mktemp) && echo "13/14 + 1/11 = 20/20" | enscript -B -f Courier-Bold16 -o- | ps2pdf - "$tmpfile" && qpdf 4568032_assignsubmission_file.A4.SCALED.pdf --overlay "$tmpfile" -- out_oneline.pdf
}


function xopp2pdf {
	echo -n "Are you sure you want to export all Xournal++ files to PDF? [y/N]: "
	read CONTINUE_FLAG
	if [ "${CONTINUE_FLAG}" = "y" ]; then
		find . -type f -name "*.xopp" -exec sh -c 'xournalpp --create-pdf="${0%.xopp}.xournalpp.pdf" {}' {} \;
		echo "Done! Please verify any errors."
	else
		echo "Ok, then. Bye!"
	fi
	# Add mark on top of the first page.
	# https://unix.stackexchange.com/a/614749
	# tmpfile=$(mktemp) && echo "13/14 + 1/11 = 20/20" | enscript -B -f Courier-Bold16 -o- | ps2pdf - "$tmpfile" && qpdf 4568032_assignsubmission_file.A4.SCALED.pdf --overlay "$tmpfile" -- out_oneline.pdf
}


while [[ "$#" -gt 0 ]]; do
    case $1 in
        --max) MAX_VALUE="$2"; shift ;;
        --maxextra) MAX_VALUE_EXTRA="$2"; shift ;;
        --assignment) ASSIGNMENT_NUMBER="$2"; shift ;;
        --scale) scale_all; ;;
        --grade) final_grade_all; ;;
        --xopp2pdf) xopp2pdf; ;;
        --help) echo "Options: “--scale” or “--max X --maxextra Y --assignment Z --grade” or “--xopp2pdf”"; ;;
        *) echo "Unknown parameter passed: $1"; exit 1 ;;
    esac
    shift
done
