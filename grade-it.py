#!/usr/bin/env python

import os
import sys
import subprocess
import fitz

input_file = sys.argv[1]
assignment_number = sys.argv[2]
expected_max_value = sys.argv[3]
expected_max_value_extra = sys.argv[4]

OUTPUT_FOLDER = "processed_files"
temp_file = "/tmp/tmp_file_marking.txt"
output_file = OUTPUT_FOLDER + "/" + str(input_file) + "_marks.txt"
output_pdf_file = OUTPUT_FOLDER + "/" + str(input_file) + "_graded.pdf"

doc = fitz.open(input_file)
pages = len(doc)

marks = list()
total_points = 0.0
maximum_points_possible = 0
maximum_points_possible_extra = 0

all_good_flag = True

for page in doc:
    text = page.getText()
    output = text.strip()
    output_list = output.split("\n")
    for line in output_list:
        if len(line) > 0:
            if line[0] == "#":
                marks.append(line)
                try:
                    item, mark_str = line.split(": ")
                except:
                    print(f"!!! Wrong grade string ({line}) for {input_file}")
                    all_good_flag = False
                    break
                points_earned, max_points = mark_str.split("/")
                total_points += float(points_earned)
                if "extra" in line:
                    maximum_points_possible_extra += float(max_points)
                else:
                    maximum_points_possible += float(max_points)


if float(expected_max_value) != maximum_points_possible:
    all_good_flag = False
    print(
        f"!!! Wrong maximum points ({maximum_points_possible}≠{expected_max_value}) for {input_file}"
    )
if maximum_points_possible_extra != 0:
    if float(expected_max_value_extra) != maximum_points_possible_extra:
        all_good_flag = False
        print(
            f"!!! Wrong maximum points with extra ({maximum_points_possible_extra}≠{expected_max_value_extra}) for {input_file}"
        )
if all_good_flag == True:
    if maximum_points_possible_extra != 0:
        maximum_points_possible += maximum_points_possible_extra
    rounded_mark = round(total_points, 2)
    final_mark_string = f"{rounded_mark}/{maximum_points_possible:.1f}"
    marks_str = "\n".join(marks)
    report = f"{final_mark_string}\n\n{marks_str}\n"

    os.makedirs(OUTPUT_FOLDER, 0o700, exist_ok=True)

    with open(output_file, "w") as file:
        file.write(report)

    header = rf"\hfill\ttfamily\bfseries Assignment #{int(assignment_number)}: {final_mark_string}"
    with open(temp_file, "w") as file:
        file.write(header)
    command = f"""tmpfile=$(mktemp) && pandoc -f markdown -t pdf -V papersize=a4 -V geometry:"top=0.25cm,left=0.25cm,right=0.25cm" -V fontsize=16pt -V documentclass=scrartcl -V pagestyle=empty -V 'fontfamilyoptions:lining' -V 'fontfamily:FiraMono' {temp_file} -o $tmpfile && qpdf {input_file} --overlay $tmpfile -- {output_pdf_file}"""
    # command = f"tmpfile=$(mktemp) && echo 'Assignment {assignment_number}: {final_mark_string}' | enscript -B -f Courier-Bold16 -o- | ps2pdf - '$tmpfile' && qpdf {input_file} --overlay '$tmpfile' -- {output_pdf_file}"
    return_value = subprocess.call(command, shell=True)
    if return_value != 0:
        print(f"!!! Error stamping {input_file}")
    else:
        summary_line = f"{rounded_mark}\t{input_file}\n"
        with open("0_summary", "a") as file:
            file.write(summary_line)
